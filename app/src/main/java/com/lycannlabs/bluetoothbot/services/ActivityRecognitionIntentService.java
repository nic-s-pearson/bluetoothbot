package com.lycannlabs.bluetoothbot.services;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.lycannlabs.bluetoothbot.models.Events;
import com.lycannlabs.bluetoothbot.utils.Constants;
import com.lycannlabs.bluetoothbot.R;
import com.lycannlabs.bluetoothbot.utils.Utils;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import de.greenrobot.event.EventBus;

public class ActivityRecognitionIntentService extends IntentService {

    // Store the applications shared preferences repository
    private SharedPreferences mPrefs;

    public static final String RESPONSE_ACTIVITY_TYPE = "activity_type";
    public static final String RESPONSE_ACTIVITY_NAME = "activity_name";
    public static final String RESPONSE_ACTIVITY_CONFIDENCE = "confidence";
    public static final String RESPONSE_IS_MOVING = "is_moving";

    public ActivityRecognitionIntentService() {
        // Set the label for the service's background thread
        super("ActivityRecognitionIntentService");
    }

    /**
     * Called when a new activity detection update is available.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Get a handle to the repository
        mPrefs = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // If the intent contains an update
        if (ActivityRecognitionResult.hasResult(intent)) {
            // Get the update
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

            // Get the most probable activity from the list of activities in the update
            DetectedActivity mostProbableActivity = result.getMostProbableActivity();

            // Get the confidence percentage for the most probable activity
            int confidence = mostProbableActivity.getConfidence();

            if (confidence > 60) {
                // Publish the update
                publishActivityRecognitionResult(result);
            }
        }
    }

    /**
     * Post a notification to the user. The notification prompts the user to click it to
     * open the device's GPS settings
     */
    private void sendNotification() {
        // Create a notification builder that's compatible with platforms >= version 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

        PendingIntent pendingIntent = getContentIntent();

        if (pendingIntent != null) {
            // Set the title, text, and icon
            builder.setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.turn_on_music))
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentIntent(pendingIntent);

            // Get an instance of the Notification Manager
            NotificationManager notifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // Build the notification and post it
            notifyManager.notify(0, builder.build());
        }
    }
    /**
     * Get a content Intent for the notification
     *
     * @return A PendingIntent that starts playing music.
     */
    private PendingIntent getContentIntent() {
        // Check the API Level
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            // Set the Intent action to play music
            Intent intent = Utils.getResumeMusicIntent(getApplicationContext());
            // Create a PendingIntent to start an Activity
            return PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            // Set the Intent action to play music
            //Intent intent = Utils.getResumeMusicIntentKitKat(getApplicationContext());
            // Create a PendingIntent to send a Broadcast
            //return PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return null;
    }

    /**
     * Tests to see if the activity has changed
     *
     * @param currentType The current activity type
     * @return true if the user's current activity is different from the previous most probable
     * activity; otherwise, false.
     */
    private boolean activityChanged(int currentType) {
        // Get the previous type, otherwise return the "unknown" type
        int previousType = mPrefs.getInt(Constants.KEY_PREVIOUS_ACTIVITY_TYPE, DetectedActivity.UNKNOWN);

        // If the previous type isn't the same as the current type, the activity has changed
        if (previousType != currentType) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine if an activity means that the user is moving.
     *
     * @param type The type of activity the user is doing (see DetectedActivity constants)
     * @return true if the user seems to be moving from one location to another, otherwise false
     */
    private boolean isMoving(int type) {
        switch (type) {
            // These types mean that the user is probably not moving
            case DetectedActivity.STILL :
            case DetectedActivity.TILTING :
            case DetectedActivity.UNKNOWN :
                return false;
            default:
                return true;
        }
    }

    /**
     * Write the activity recognition update to the log file

     * @param result The result extracted from the incoming Intent
     */
    private void publishActivityRecognitionResult(ActivityRecognitionResult result) {
        // Get all the probably activities from the updated result
        for (DetectedActivity detectedActivity : result.getProbableActivities()) {

            // Get the activity type, confidence level, and human-readable name
            int activityType = detectedActivity.getType();
            int confidence = detectedActivity.getConfidence();
            String activityName = Utils.getNameFromType(activityType);

            if (confidence > 60) {
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtra(RESPONSE_ACTIVITY_TYPE, activityType);
                broadcastIntent.putExtra(RESPONSE_ACTIVITY_NAME, activityName);
                broadcastIntent.putExtra(RESPONSE_ACTIVITY_CONFIDENCE, confidence);
                broadcastIntent.putExtra(RESPONSE_IS_MOVING, isMoving(activityType));

                // Check to see if the repository contains a previous activity
                if (!mPrefs.contains(Constants.KEY_PREVIOUS_ACTIVITY_TYPE)) {
                    // This is the first time an activity has been detected. Store the type
                    Editor editor = mPrefs.edit();
                    editor.putInt(Constants.KEY_PREVIOUS_ACTIVITY_TYPE, activityType);
                    editor.apply();
                } else if (
                    // If the current type is "moving"
                    isMoving(activityType) &&
                    // The activity has changed from the previous activity
                    activityChanged(activityType) &&
                     // The confidence level for the current activity is > 60%
                    confidence >= 60) {
                        try {
                            // Publish the intent
                            EventBus.getDefault().postSticky(new Events.ActivityRecognitionReceived(broadcastIntent));
                            sendNotification();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    }
}