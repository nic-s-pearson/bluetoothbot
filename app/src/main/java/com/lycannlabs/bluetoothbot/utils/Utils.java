package com.lycannlabs.bluetoothbot.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.Window;

import com.google.android.gms.location.DetectedActivity;
import com.lycannlabs.bluetoothbot.R;

public class Utils {

    private static ProgressDialog _dialog;

    public static void showInProgress(Activity activity, String msg) {
        if (_dialog != null) {
            hideInProgress();
        }

        if (_dialog == null) {
            String displayMsg = msg != null ? msg : activity.getResources().getString(R.string.msg_loading);
            _dialog = new ProgressDialog(activity);
            _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            _dialog.setMessage(displayMsg);
            _dialog.setIndeterminate(true);
            _dialog.setCancelable(false);
            _dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            try {
                _dialog.show();
            } catch (Exception e) {
                hideInProgress();
            }
        }
    }

    public static void hideInProgress() {
        if (_dialog != null) {
            try {
                _dialog.dismiss();
                _dialog = null;
            } catch (Exception e) {
                _dialog = null;
            }
        }
    }

    /**
     * Map detected activity types to strings
     *
     * @param activityType The detected activity type
     * @return A user-readable name for the type
     */
    public static String getNameFromType(int activityType) {
        switch (activityType) {
            case DetectedActivity.IN_VEHICLE:
                return "Driving";
            case DetectedActivity.ON_BICYCLE:
                return "Cycling";
            case DetectedActivity.ON_FOOT:
            case DetectedActivity.WALKING:
                return "Walking";
            case DetectedActivity.RUNNING:
                return "RUNNING";
            case DetectedActivity.STILL:
                return "Stationary";
            case DetectedActivity.UNKNOWN:
                return "Stationary";
            case DetectedActivity.TILTING:
                return "Stationary";
        }
        return "Stationary";
    }

    /**
     * Map detected activity types to the corresponding image
     *
     * @param activityType The detected activity type
     * @return A resource id for the type
     */
    public static int getActivityRecognitionDrawableFromType(int activityType) {
        switch (activityType) {
            case DetectedActivity.IN_VEHICLE:
                return R.drawable.driving_gauge;
            case DetectedActivity.ON_BICYCLE:
                return R.drawable.cycling_gauge;
            case DetectedActivity.ON_FOOT:
            case DetectedActivity.WALKING:
                return R.drawable.walking_gauge;
            case DetectedActivity.RUNNING:
                return R.drawable.running_gauge;
            case DetectedActivity.STILL:
            case DetectedActivity.UNKNOWN:
            case DetectedActivity.TILTING:
                return R.drawable.still_gauge;
        }
        return R.drawable.still_gauge;
    }

    public static Intent getResumeMusicIntent(Context context) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (!mAudioManager.isMusicActive()) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra("command", "play");
            return intent;
        }
        return null;
    }

    @TargetApi(19)
    public static void getResumeMusicIntentKitKat(Context context) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if (!mAudioManager.isMusicActive()) {
            long eventTime = SystemClock.uptimeMillis() - 1;
            KeyEvent downEvent = new KeyEvent(eventTime, eventTime, KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY, 0);
            mAudioManager.dispatchMediaKeyEvent(downEvent);

            eventTime++;
            KeyEvent upEvent = new KeyEvent(eventTime, eventTime, KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PLAY, 0);
            mAudioManager.dispatchMediaKeyEvent(upEvent);
        }
    }
}
