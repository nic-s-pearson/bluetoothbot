package com.lycannlabs.bluetoothbot.utils;

public final class Constants {

    // Used to track what type of request is in process
    public enum REQUEST_TYPE {ADD, REMOVE}

    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    /*
     * ACTIVITY RECOGNITION INTERVALS
     */
    public static final int DETECTION_INTERVAL_SECONDS = 20;
    public static final int DETECTION_INTERVAL_MILLISECONDS = 1000 * DETECTION_INTERVAL_SECONDS;

    /*
     * PREFS
     */
    public static final String SHARED_PREFERENCES = "com.lycannlabs.bluetoothbot.SHARED_PREFERENCES";
    public static final String KEY_PREVIOUS_ACTIVITY_TYPE = "com.lycannlabs.bluetoothbot.KEY_PREVIOUS_ACTIVITY_TYPE";
}
