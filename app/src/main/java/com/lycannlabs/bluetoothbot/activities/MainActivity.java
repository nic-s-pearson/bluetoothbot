package com.lycannlabs.bluetoothbot.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.lycannlabs.bluetoothbot.fragments.HomeFragment;
import com.lycannlabs.bluetoothbot.fragments.NavigationDrawerFragment;
import com.lycannlabs.bluetoothbot.R;
import com.lycannlabs.bluetoothbot.receivers.BluetoothBroadcastReceiver;
import com.lycannlabs.bluetoothbot.utils.Constants;
import com.lycannlabs.bluetoothbot.utils.DetectionRemover;
import com.lycannlabs.bluetoothbot.utils.DetectionRequester;
import com.lycannlabs.bluetoothbot.utils.Utils;

public class MainActivity extends FragmentActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = "MainActivity";

    private NavigationDrawerFragment mNavigationDrawerFragment;

    // Used to store the last screen title
    private CharSequence mTitle;

    // Store the current request type (ADD or REMOVE)
    private Constants.REQUEST_TYPE mRequestType;

    // The activity recognition update request object
    private DetectionRequester mDetectionRequester;

    // The activity recognition update removal object
    private DetectionRemover mDetectionRemover;

    private BluetoothBroadcastReceiver mBluetoothBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        mBluetoothBroadcastReceiver = new BluetoothBroadcastReceiver();
        registerReceiver(mBluetoothBroadcastReceiver, filter);

        // Get detection requester and remover objects
        mDetectionRequester = new DetectionRequester(this);
        mDetectionRemover = new DetectionRemover(this);

        // Start updates and register the receiver
        onStartUpdates();

        // Navigate to the first view, i.e. home
        displayView(0);
    }

    /*
     * Handle results returned to this Activity by other Activities started with
     * startActivityForResult(). In particular, the method onConnectionFailed() in
     * DetectionRemover and DetectionRequester may call startResolutionForResult() to
     * start an Activity that handles Google Play services problems. The result of this
     * call returns here, to onActivityResult.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // Choose what to do based on the request code
        switch (requestCode) {
            // If the request code matches the code sent in onConnectionFailed
            case Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST:
                switch (resultCode) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:
                        // If the request was to start activity recognition updates
                        if (Constants.REQUEST_TYPE.ADD == mRequestType) {
                            // Restart the process of requesting activity recognition updates
                            mDetectionRequester.requestUpdates();
                        // If the request was to remove activity recognition updates
                        } else if (Constants.REQUEST_TYPE.REMOVE == mRequestType) {
                            // Restart the removal of all activity recognition updates for the PendingIntent.
                            mDetectionRemover.removeUpdates(mDetectionRequester.getRequestPendingIntent());
                        }
                        break;
                    // If any other result was returned by Google Play services
                    default:
                        // Report that Google Play services was unable to resolve the problem.
                        Log.d(TAG, getString(R.string.no_resolution));
                }
                // If any other request code was received
            default:
                // Report that this Activity received an unknown requestCode
                Log.d(TAG, getString(R.string.unknown_activity_request_code, requestCode));
                break;
        }
    }

    /*
     * Unregister the receiver on destroy
     */
    @Override
    public void onDestroy() {
        onStopUpdates();
        unregisterReceiver(mBluetoothBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        displayView(position);
    }

    /**
     * Display the fragment of the selected navigation drawer list item
     */
    public void displayView(int position) {
        Utils.showInProgress(this, this.getResources().getString(R.string.msg_loading));

        switch (position)
        {
            case 0:
                getFragmentForDisplay(HomeFragment.class, true);
                break;

            default:
                break;
        }

        Utils.hideInProgress();
    }

    /**
     * Replaces the current fragment with a new fragment,
     * optionally adding the old fragment to the back stack
     *
     * @param classFragment The fragment class
     * @param pushOnBackStack Whether or not to add fragment to the back stack
     */
    public Fragment navigateToFragmentSimple(Class<? extends Fragment> classFragment, boolean pushOnBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = Fragment.instantiate(this, classFragment.getName());
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (pushOnBackStack)  {
            ft.addToBackStack(classFragment.getName());
        }
        ft.replace(R.id.mainContainer, fragment, classFragment.getName());
        ft.commit();
        return fragment;
    }

    /**
     * Return the fragment if it already exists and is visible,
     * else instantiate and navigate to it
     *
     * @param classFragment The fragment class
     * @param pushOnBackStack Whether or not to add fragment to the back stack
     */
    public Fragment getFragmentForDisplay(Class<? extends Fragment> classFragment, boolean pushOnBackStack) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(classFragment.getName());
        if (fragment == null || (!fragment.isVisible())) {
            fragment = navigateToFragmentSimple(classFragment, pushOnBackStack);
        }
        return fragment;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /*
     * Handle selections from the menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {

            case R.id.menu_item_start_updates:
                onStartUpdates();
                return true;

            case R.id.menu_item_stop_updates:
                onStopUpdates();
                return true;

            case R.id.menu_item_about:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0).show();
            return false;
        }
    }

    /**
     * Respond to "Start" button by requesting activity recognition updates.
     */
    public void onStartUpdates() {
        // Check for Google Play services
        if (!servicesConnected()) {
            return;
        }

        /*
         * Set the request type. If a connection error occurs, and Google Play services can
         * handle it, then onActivityResult will use the request type to retry the request
         */
        mRequestType = Constants.REQUEST_TYPE.ADD;

        // Pass the update request to the requester object
        mDetectionRequester.requestUpdates();
    }

    /**
     * Respond to "Stop" button by canceling updates.
     */
    public void onStopUpdates() {

        // Check for Google Play services
        if (!servicesConnected()) {
            return;
        }

        /*
         * Set the request type. If a connection error occurs, and Google Play services can
         * handle it, then onActivityResult will use the request type to retry the request
         */
        mRequestType = Constants.REQUEST_TYPE.REMOVE;

        // Pass the remove request to the remover object
        mDetectionRemover.removeUpdates(mDetectionRequester.getRequestPendingIntent());

        /*
         * Cancel the PendingIntent. Even if the removal request fails, canceling the PendingIntent
         * will stop the updates.
         */
        mDetectionRequester.getRequestPendingIntent().cancel();
    }
}
