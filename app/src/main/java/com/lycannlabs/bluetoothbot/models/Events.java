package com.lycannlabs.bluetoothbot.models;

import android.content.Intent;

public class Events {

    public static abstract class E {
        public E() {
            // Empty
        }
    }

    public static class ActivityRecognitionReceived extends E {
        private Intent intent;

        public ActivityRecognitionReceived(Intent intent) {
            super();
            this.intent = intent;
        }

        public Intent getIntent() {
            return this.intent;
        }
    }

    public static class BluetoothStateReceived extends E {
        private int state;

        public BluetoothStateReceived(int state) {
            super();
            this.state = state;
        }

        public int getState() {
            return this.state;
        }
    }
}
