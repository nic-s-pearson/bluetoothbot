package com.lycannlabs.bluetoothbot.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lycannlabs.bluetoothbot.R;
import com.lycannlabs.bluetoothbot.models.Events;
import com.lycannlabs.bluetoothbot.services.ActivityRecognitionIntentService;
import com.lycannlabs.bluetoothbot.utils.Constants;
import com.lycannlabs.bluetoothbot.utils.Utils;

import de.greenrobot.event.EventBus;

public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    private static ImageView bluetoothImageView, activityImageView;

    private static Boolean bluetoothConnected = false, bluetoothRequired = false;

    @Override
    public void onCreate(Bundle savedInstanceState)	{
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setRetainInstance(true);
        setHasOptionsMenu(true);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // Get a handle to the bluetooth updates
        bluetoothImageView = (ImageView) view.findViewById(R.id.bluetoothImageView);

        // Get a handle to the activity updates
        activityImageView = (ImageView) view.findViewById(R.id.activityImageView);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /*
     * Register the broadcast receiver and update the log of activity updates
     */
    @Override
    public void onResume() {
        super.onResume();
        // Load updated activity history
        updateActivityHistory();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    /**
     * Display the activity detection history stored in the log file
     */
    private void updateActivityHistory() {
        // Try to load data from the history file
        try {
            SharedPreferences mPrefs = getActivity().getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            if (mPrefs.contains(Constants.KEY_PREVIOUS_ACTIVITY_TYPE)) {
                int lastActivityType = mPrefs.getInt(Constants.KEY_PREVIOUS_ACTIVITY_TYPE, 0);
                activityImageView.setImageDrawable(getActivity().getResources().getDrawable(Utils.getActivityRecognitionDrawableFromType(lastActivityType)));
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    /*
     * Event Bus Subscriptions
     */
    public void onEventMainThread(Events.ActivityRecognitionReceived e) {
        if (e != null && e.getIntent() != null) {

            Intent intent = e.getIntent();
            int responseActivityType = intent.getIntExtra(ActivityRecognitionIntentService.RESPONSE_ACTIVITY_TYPE, 0);
            Boolean isMoving = intent.getBooleanExtra(ActivityRecognitionIntentService.RESPONSE_IS_MOVING, false);

            if (activityImageView != null) {
                activityImageView.setImageDrawable(getActivity().getResources().getDrawable(Utils.getActivityRecognitionDrawableFromType(responseActivityType)));
            }

            // Only set bluetooth if moving
            if (isMoving) {
                setBluetooth(true);
            }
        }
    }

    public void onEventMainThread(Events.BluetoothStateReceived e) {
        if (e != null && e.getState() >= 0) {
            int state = e.getState();
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    bluetoothConnected = false;
                    bluetoothImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.bluetooth_off));
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    bluetoothConnected = false;
                    bluetoothImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.bluetooth_off));
                    break;
                case BluetoothAdapter.STATE_ON:
                    bluetoothImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.bluetooth_on));
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    bluetoothImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.bluetooth_on));
                    break;
                case BluetoothAdapter.STATE_CONNECTED:
                    bluetoothConnected = true;
                    break;
            }
        }
    }

    /*
     * Set Bluetooth Adapter
     */
    public boolean setBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }

        if (bluetoothAdapter.isEnabled()) {
            launchMediaPlayer();
        }
        // No need to change bluetooth state
        return true;
    }

    public void launchMediaPlayer() {
        if (bluetoothRequired && bluetoothConnected) {
            // Check the API Level
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                // Set the Intent action to play music
                Intent intent = Utils.getResumeMusicIntent(getActivity().getApplicationContext());
                // Start playing music
                getActivity().sendBroadcast(intent);
            } else {
                // Start playing music
                Utils.getResumeMusicIntentKitKat(getActivity().getApplicationContext());
            }
        }
    }
}

